package com.upricer.se;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UpricerApiApplication {

	public static void main(String[] args) {
		System.out.println("Starting Upricer applcation");
		SpringApplication.run(UpricerApiApplication.class, args);
	}

}
